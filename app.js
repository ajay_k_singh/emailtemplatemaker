
const bodyParser = require('body-parser');
const express = require('express');
const fs = require('fs');

const app = express();
const port = 3000;

app.set('views', './views'); // specify the views directory
app.set('view engine', 'ejs'); // register the template engine

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/views'));
app.use( express.static("public") );

const readJson = fs.readFileSync('./data/series.json');
const data = JSON.parse(readJson);
app.get('/', (req, res) => {
  res.render('index', { ...data[0].order });
});

app.listen(port, () => console.log(`json-bread listening on port ${port}!`));